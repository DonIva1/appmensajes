/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.todomining.appmensajes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Lenovo
 */
public class Conexion {
    
    public Connection getConexion(){
        Connection connection = null;
        try{
            connection = DriverManager.getConnection("jdbc:mysql://localhost/mensaje_app", "root", "");        
            if(connection != null){
                System.out.println("Conexión exitosa");
            }
        } catch(SQLException e){
            System.out.println(e);
        }
        
        return connection;
    }
    
}
